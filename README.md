## Django Development With Docker Compose and Machine

Featuring:

- Docker v1.10.3
- Docker Compose v1.6.2
- Docker Machine v0.6.0
- Python 3.5

Blog post -> https://realpython.com/blog/python/django-development-with-docker-compose-and-machine/

### OS X Instructions to dev environment

1. Start new machine - `docker-machine create -d virtualbox cvcube-dev;`
1. Point docker at the cvcube_dev machine - `eval $(docker-machine env cvcube-dev)`
1. Build images - `docker-compose build`
1. Start services - `docker-compose up -d`
1. Create migrations - `docker-compose run --rm web ./web/manage.py migrate`
1. Grab IP - `docker-machine ip cvcube-dev` - and view in your browser

### Clean untagged images
1.`docker rmi -f $(docker images | grep "<none>" | awk "{print \$3}")`
1. Remove Orphans 'docker rmi \`docker images | awk '{ print $3; }'\``
1. Remove all stopped containers `docker ps -q |xargs docker rm`
