# -*- coding: utf-8 -*-
__author__ = 'jnotaro'
import json
from django.views.generic.base import TemplateView
from django.views.generic import ListView, DetailView
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseBadRequest
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404, render_to_response
from django.utils.translation import gettext as _
#from django.views.generic import ListView,DetailView
from django.core.mail import send_mail
from apps.ajaweb.models import *
#from .forms import ContactForm


"""

class CategoryView(TemplateView):
    template_name = 'category.html'


    def get_context_data(self, slug, parent_slug='', **kwargs):
        context = super(CategoryView, self).get_context_data(**kwargs)

        self.slug = slug
        self.parent_slug = parent_slug

        if slug:
            try:
                category = Category.objects.filter(slug=slug)[0]
                projects = Project.objects.filter(categories__pk = category.pk, active=True)

            except Category.DoesNotExist:
                return HttpResponseRedirect(reverse('404'))

            child_categories = category.get_all_children()
        else:
            category = Category.objects.active()
            products = Project.objects.filter(active=True)
            child_categories = category.get_all_children()

        context = {
            'category': category,
            'child_categories': child_categories,
            #'sale' : sale,
            'projects' : projects,
            'project_amount': len(projects),
        }

        return context

class ProjectView(ListView):
    template_name = 'project/project_list.html'
    model = Project


class ProjectDetailView(DetailView):

    model = Project
    context_object_name = 'project'
    template_name = 'project/detail.html'


    def get_queryset(self):
        return super(ProjectDetailView,self).get_queryset().filter(active=True)

class ThanksView(TemplateView):
     template_name = 'thanks.html'


"""

# class ContactView(TemplateView):
#     template_name = 'contact.html'
#     form = ContactForm
#
#     def dispatch(self, *args, **kwargs):
#         return super(ContactView, self).dispatch(*args, **kwargs)
#
#     def post(self, request, *args, **kwargs):
#
#         kwargs.update({
#                        'name': request.POST.get('name', None),
#                        'email': request.POST.get('email', None),
#                        'phone': request.POST.get('phone', None),
#                        'message': request.POST.get('message', None),
#                        })
#         if not request.is_ajax():
#
#             in_data = json.loads(json.dumps(request.POST))
#             self.form = ContactForm(data=in_data)
#         else:
#             self.form = ContactForm(data=request.POST)
#
#         if self.form.is_valid():
#             #print 'yes done'
#             cd = self.form.cleaned_data
#
#             email_to = 'info@jeffersonnotaro.ch'
#             email_from = cd['email']
#             subject = "{0} Customer: ".format(
#                 cd['name'])
#             message = "Message: {0}\n\n ".format(
#                 cd['message'] + "\n\nEmail:" + cd['email'] + "\n\nTelefone:" + cd['phone'])
#             send_mail(subject, message, email_from,[email_to,])
#             self.form.save()
#
#             return HttpResponseRedirect('/danke/')
#         else:
#
#             self.form.initial = kwargs
#
#         return super(ContactView,self).get(request,*args,**kwargs)
#
#     def get(self, request, *args, **kwargs):
#         self.contact = Contact.objects.all()
#         self.form = ContactForm(data=request.POST or None)
#         return super(ContactView,self).get(request,*args,**kwargs)
#
#     def get_context_data(self, **kwargs):
#
#         if len(kwargs)>0:
#             self.form.initial = kwargs
#         return {
#             'form' : self.form,
#         }