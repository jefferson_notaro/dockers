# -*- coding: utf-8 -*-
import datetime
from django.db import models
from django.utils.translation import gettext as _
from django.core.urlresolvers import reverse

from django.db.models import Q
from django.template.defaultfilters import slugify
from .utils import get_flat_list

"""

class CategoryManager(models.Manager):

    def active(self, **kwargs):
        return self.filter(is_active=True, **kwargs)

    def by_product_object(self, _product_object=None, **kwargs):

        if not _product_object:
            return self.active(**kwargs)

        return self.active(product_object = _product_object, **kwargs)

    def get_by_product_object(self, _product_object=None, **kwargs):
        if not _product_object:
            return self.active(**kwargs)
        return self.active().get(product_object = _product_object, **kwargs)

    def root_categories(self, **kwargs):


        return self.active(parent__isnull=True, **kwargs)

    def search_by_product_object(self, keyword, _product_object=None, include_children=False):


        if not _product_object:
            cats = self.active().filter(
                Q(name__icontains=keyword) |
                Q(meta__icontains=keyword) |
                Q(description__icontains=keyword))
        else:
            cats = self.active().filter(
                Q(name__icontains=keyword) |
                Q(meta__icontains=keyword) |
                Q(description__icontains=keyword),
                product_object=_product_object)

        if include_children:
            # get all the children of the categories found
            cats = [cat.get_active_children(include_self=True) for cat in cats]

        # sort properly
        if cats:
            fastsort = [(c.ordering, c.name, c) for c in get_flat_list(cats)]
            fastsort.sort()
            # extract the cat list
            cats = zip(*fastsort)[2]
        return cats

class Category(models.Model):

    name = models.CharField(_("Name"), max_length=200)
    slug = models.SlugField(_("Slug"), blank=True, max_length=255)
    #product_object = models.CharField(max_length=200, choices=PRODUCT_OBJECT_CHOICE, verbose_name='Produkt Objekt')
    parent = models.ForeignKey('self', blank=True, null=True,
        related_name='child')
    meta = models.TextField(_("Meta Beschreibung"), blank=True, null=True,
        help_text=_("Meta Beschreibung für diese Kategorie"))
    description = models.TextField(_("Beschreibung"), blank=True,
        help_text="Optional")
    ordering = models.IntegerField(_("Reihenfolge"), default=0, help_text=_("Überschreiben Sie alphabetischer Reihenfolge in der Kategorie Anzeige"))
    is_active = models.BooleanField(_("Aktive"), default=True, blank=True)
    related_categories = models.ManyToManyField('self', blank=True, null=True,
        verbose_name=_('In Verbindung stehende Kategorie'), related_name='related_categories')
    #objects = CategoryManager()
    #models.ImageField(upload_to=)
    def _recurse_for_parents(self, cat_obj):
        p_list = []
        if cat_obj.parent_id:
            p = cat_obj.parent
            p_list.append(p)
            if p != self:
                more = self._recurse_for_parents(p)
                p_list.extend(more)
        if cat_obj == self and p_list:
            p_list.reverse()
        return p_list

    def get_separator(self):
        return ' :: '

    def __unicode__(self):
        name_list = [cat.name for cat in self._recurse_for_parents(self)]
        name_list.append(self.name)
        return self.get_separator().join(name_list)

    def save(self, **kwargs):

        if self.name and not self.slug:
            self.slug = slugify(self.name)
        super(Category, self).save(**kwargs)

    def get_absolute_url(self):
        parents = self._recurse_for_parents(self)
        slug_list = [cat.slug for cat in parents]
        if slug_list:
            slug_list = "/".join(slug_list) + "/"
        else:
            slug_list = ""
        return reverse('jn_category',
            kwargs={'parent_slugs' : slug_list, 'slug' : self.slug})

    def active_products(self, variations=False, include_children=False, **kwargs):

        if not include_children:
            qry = SeelenBalsamProduct.objects.all()
        else:
            cats = self.get_all_children(include_self=True)
            qry = SeelenBalsamProduct.objects.filter(category__in=cats)

        if variations:
            slugs = qry.filter(site=self.site, active=True, **kwargs).values_list('slug',flat=True)
            return Product.objects.filter(Q(productvariation__parent__product__slug__in = slugs)|Q(slug__in = slugs)).prefetch_related('productimage_set')
        else:
            return qry.filter(active=True, **kwargs)

    def _flatten(self, L):

        if type(L) != type([]): return [L]
        if L == []: return L
        return self._flatten(L[0]) + self._flatten(L[1:])

    def _recurse_for_children(self, node, only_active=False):
        children = []
        children.append(node)
        for child in node.child.active():
            if child != self:
                if (not only_active) or child.active_products().count() > 0:
                    children_list = self._recurse_for_children(child, only_active=only_active)
                    children.append(children_list)
        return children

    def get_active_children(self, include_self=False):

        return self.get_all_children(only_active=True, include_self=include_self)

    def get_all_children(self, only_active=False, include_self=False):

        children_list = self._recurse_for_children(self, only_active=only_active)
        if include_self:
            ix = 0
        else:
            ix = 1
        flat_list = self._flatten(children_list[ix:])
        return flat_list

    class Meta:
        ordering = ['parent__ordering', 'parent__name', 'ordering', 'name']
        verbose_name = _("Kategorie")
        verbose_name_plural = _("Kategorien")

class CategoryImage(models.Model):

    category = models.ForeignKey(Category, null=True, blank=True,
        related_name="images")
    picture = models.ImageField(upload_to='images/category/%Y/%m/%d', verbose_name=_('Bilder'))
    caption = models.CharField(_("Optional Beschriftung"), max_length=100,
        null=True, blank=True)
    sort = models.IntegerField(_("Reihenfolge"), default=0)


    def _get_filename(self):
        if self.category:
            return '%s-%s' % (self.category.slug, self.id)
        else:
            return 'default'
    _filename = property(_get_filename)

    def __unicode__(self):
        if self.category:
            return u"Bidl von Kategorie %s" % self.category.slug
        elif self.caption:
            return u"Bild mit Beschriftung \"%s\"" % self.caption
        else:
            return u"%s" % self.picture

    class Meta:
        ordering = ['sort']
        unique_together = (('category', 'sort'),)
        verbose_name = _("Kategorie Bild")
        verbose_name_plural = _("Kategorien Bilder")

class Project(models.Model):

    name = models.CharField(max_length=255, verbose_name=_('Project Titel'), null=True, blank=True)
    slug = models.SlugField(_("Slug"), blank=True, max_length=255)
    categories = models.ManyToManyField(Category, blank=True, verbose_name=_("Kategorie"), related_name='related_categories_product')
    projectnumber = models.CharField(max_length=255, verbose_name=_('Projekt Nummer'))
    description = models.TextField(verbose_name=_('Beschreibung'))
    description_detail = models.TextField(verbose_name=_('Beschreibung Detail'), null=True, blank=True)
    urllink = models.URLField(verbose_name=_('URL Link'), null=True, blank=True)
    active = models.BooleanField(default=True, verbose_name=_('Kaufbar'))
    related_items = models.ManyToManyField('self', blank=True, null=True, verbose_name=_('Ähnliche Produkte'), related_name='related_products')

    def get_absolute_url(self):
        return reverse('jn_project_detail',kwargs={'slug':self.slug, 'category':Category.objects.get(pk=self.categories._fk_val).name})

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ['name']
        verbose_name = _('Projekt')
        verbose_name_plural = _('Projekte')

class ProjectImage(models.Model):

    project = models.ForeignKey(Project, null=True, blank=True)
    picture = models.ImageField(upload_to='images/product/%Y/%m/%d', verbose_name=_('Bilder'))
    caption = models.CharField(_("Optional caption"), max_length=100,
        null=True, blank=True)
    sort = models.IntegerField(_("Sort Order"), default=0)



    def _get_filename(self):
        if self.product:
            # In some cases the name could be too long to fit into the field
            # Truncate it if this is the case
            pic_field_max_length = self._meta.get_field('picture').max_length
            max_slug_length = pic_field_max_length -len('images/productimage-picture-.jpg') - len(str(self.id))
            slug = self.product.slug[:max_slug_length]
            return '%s-%s' % (slug, self.id)
        else:
            return 'default'
    _filename = property(_get_filename)

    def __unicode__(self):
        if self.product:
            return u"Image of Product %s" % self.product.slug
        elif self.caption:
            return u"Image with caption \"%s\"" % self.caption
        else:
            return u"%s" % self.picture

    class Meta:
        ordering = ['sort']
        verbose_name = _("Projekttbild")
        verbose_name_plural = _("Projektabbildungen")

class Article(models.Model):
    title = models.CharField(
        verbose_name = _(u'Title'),
        help_text = _(u' '),
        max_length = 255
    )
    slug = models.SlugField(
        verbose_name = _(u'Slug'),
        help_text = _(u'Uri identifier.'),
        max_length = 255,
        unique = True
    )

    date_publish = models.DateTimeField(
        verbose_name = _(u'Publish Date'),
        help_text = _(u' ')
    )
    content = models.TextField()

    class Meta:
        verbose_name = _(u"Article")
        verbose_name_plural = _(u"Articles")
        ordering = ['-date_publish']

    def __unicode__(self):
        return "%s" % (self.title,)

class Contact(models.Model):

    name        = models.CharField(max_length=150, verbose_name=_('Name'),)
    email       = models.EmailField(verbose_name=_('E-mail'))
    phone       = models.CharField(max_length=150, verbose_name=_('Telefone'), blank=True, null=True)
    message     = models.TextField(verbose_name=_('Eintrag'), blank=True, null=True)
    added_date  = models.DateField(auto_now_add=True)

    class Meta:
        verbose_name = _(u"Kontakt")
        verbose_name_plural = _(u"Kontakte")
        ordering = ['name']

    def __unicode__(self):
        return "%s" % (self.name,)

"""
