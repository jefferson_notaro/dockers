# -*- coding: utf-8 -*-
# from __future__ import unicode_literals
# from django import forms
# from django.utils.translation import gettext as _
# from .models import Contact
# from django.core.validators import EmailValidator,MaxLengthValidator
# from django.utils import six
# from djangular.forms import NgDeclarativeFieldsMetaclass, NgFormValidationMixin
# from djangular NgModelFormMixin, NgModelForm
# from djangular.forms.fields import FloatField
# #from djangular.styling.bootstrap3.forms import Bootstrap3Form
#
# def validate_password(value):
#     # Just for demo. Do not validate passwords like this!
#     if value != 'secret':
#         raise ValidationError('The password is wrong.')
#
# class ContactForm(NgModelFormMixin, NgModelForm):
#     #surname = forms.CharField(label='vorname', min_length=3, max_length=20)
#     #age = forms.DecimalField(min_value=18, max_value=99)
#     def __init__(self, *args, **kwargs):
#         super(ContactForm, self).__init__(*args, **kwargs)
#         self.form_name = "appContactForm"
#         self.fields['phone'].required = False
#         self.fields["message"].required = False
#         self.fields["name"].max_length = 5
#         self.fields["name"].validators.append(MaxLengthValidator)
#         #self.fields["name"].error_messages['required'] = _("Geben Sie bitte einen Namen ein.")
#         #self.fields["message"].error_messages['required'] = _("Geben Sie bitte eine Mitteilung ein.")
#         self.fields["email"].validators.append(EmailValidator)
#
#     class Meta:
#         model = Contact
#         fields = ('name', 'email', 'phone', 'message', )
#
