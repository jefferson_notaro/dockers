# -*- coding: utf-8 -*-
from django.contrib import admin
from django.utils.translation import gettext as _
from django.contrib.admin.filters import RelatedFieldListFilter,SimpleListFilter

from django import forms
from .models import *
from apps.ajaweb.utils import AdminMixins


"""

#######################################################################################################
class CategoryImage_Inline(admin.TabularInline):
    model = CategoryImage
    extra = 3

class ProjectImage_Inline(admin.TabularInline):
    model = ProjectImage
    extra = 3


class CategoryAdmin(admin.ModelAdmin):
    model = Category
    prepopulated_fields = {'slug': ('name',)}
    ordering = ['parent__id', 'ordering', 'name']
    filter_horizontal = ('related_categories',)
    inlines = [CategoryImage_Inline]
    list_display = ('name', 'parent','is_active')
    list_filter = ['is_active']
    search_fields = ['slug', 'name', 'description']


class ProjectAdmin(admin.ModelAdmin):
    model = Project
    prepopulated_fields = {'slug': ('name',)}
    ordering = ['name']
    filter_horizontal = ('categories', 'related_items')
    list_filter = ['categories', 'active' ]
    list_display = ('name', 'projectnumber', 'active')
    list_display_links = ('name', 'projectnumber')
    inlines = [ProjectImage_Inline]
    search_fields = ['slug', 'name', 'description', 'productnumber']


class ArticleAdmin(admin.ModelAdmin):
    model = Article

class ContactAdmin(admin.ModelAdmin):
    model = Contact

admin.site.register(Category,CategoryAdmin)
admin.site.register(Project, ProjectAdmin)
admin.site.register(Article, ArticleAdmin)
admin.site.register(Contact, ContactAdmin)

"""
