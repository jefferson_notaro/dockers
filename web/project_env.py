__author__ = 'jnotaro'
import os
#Not yet used, this file will be implement as soon as we have dev environment at server side
default_environment = 'prod'

# Individual Settings
remote_env_settings = {
    'prod': {
        'server_list':['jeffersonnotaro.ch', 'www.jeffersonnotaro.ch'],
        'user':'jnotaro',
        'domain':'jeffersonnotaro.ch',
    },
    'dev': {
        'server_list':['dev.jeffersonnotaro.ch'],
        'user':'admin',
        'domain':'dev.jeffersonnotaro.ch',
    }
}

local_env_settings = {
    'static_root':'/Users/jnotaro/Sites/jeffersonnotaro.ch/static',
}

# Default settings
for env_name in remote_env_settings.keys():
    remote_env_settings[env_name]['project_directory_name'] = remote_env_settings[env_name]['domain']
    remote_env_settings[env_name]['project_root'] = os.path.join('/','home',remote_env_settings[env_name]['user'],remote_env_settings[env_name]['project_directory_name'])
    remote_env_settings[env_name]['static_root'] = os.path.join(remote_env_settings[env_name]['project_root'],'static')