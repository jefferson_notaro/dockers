__author__ = 'jnotaro'
from fabric.api import task, env, local, roles, cd, run
from fabric.contrib import project

# Where the static files get collected locally. Your STATIC_ROOT setting.
env.local_static_root = '/Users/jnotaro/Sites/jeffersonnotaro.ch/static'

# Where the static files should go remotely
env.remote_static_root = '/home/jnotaro/jeffersonnotaro.ch'
env.user = 'jnotaro'
env.hosts = ['jeffersonnotaro.ch',]
project_root = '/home/jnotaro/jeffersonnotaro.ch'

@task
def git_update():
    print "\nUpdating jeffersonnotaro.ch GIT repository ..."
    with cd(project_root):
        run('git pull');

@task
def deploy_static():
    print "\ndeploying static files ... VIRTUALENV must be activated!"
    local('python manage.py collectstatic  -v0 --noinput --settings=jeffersonnotaroweb.settings.prod')
    #local('python manage.py collectstatic  -v0 --noinput')
    project.rsync_project(
        remote_dir = env.remote_static_root,
        local_dir = env.local_static_root,
        delete = True
    )
    project_restart()

@task
def project_restart():
    print "\nRestarting jeffersonnotaro.ch ..."
    with cd('/home/jnotaro/init'):
        run('./gabrielereitemann restart')

@task
def server_restart():
    print "\nRestarting lighttpd..."
    with cd('/home/jnotaro/init'):
        run('./lighttpd restart')

@task
def migrate():
    print "\nMigrating DB changes..."
    with cd(project_root):
        run('env/bin/python manage.py migrate')
@task
def pip():
    print "\nInstall PIP packages..."
    with cd(project_root):
        run('./env/bin/pip install -r requirements/prod.txt')

@task
def import_product():
    print "\nImport Products ..."
    git_update()
    with cd(project_root):
        run('./env/bin/python manage.py import_product');

@task
def deploy():
    git_update()
    pip()
    migrate()
    deploy_static()
    project_restart()

@task
def quick_deploy():
    git_update()
    project_restart()