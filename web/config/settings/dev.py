from __future__ import absolute_import
from .common import *

# Dev overrides
# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = DEBUG
#PIPELINE = True

INSTALLED_APPS += (
#ß    'debug_toolbar',
#    'debugtools',
#    'django_nose',
#    'sentry',
)
MIDDLEWARE_CLASSES += (
       # 'debug_toolbar.middleware.DebugToolbarMiddleware',
    )
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'postgres',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        'HOST': 'postgres', #not localhost but the name we gave for the image on compose.
        'PORT': '5432',
    }
}

#EMAIL DEV
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'jefferson.notaro.emailtest@gmail.com'
EMAIL_HOST_PASSWORD = ''
DEFAULT_FROM_EMAIL = 'jefferson.notaro@gmail.com'