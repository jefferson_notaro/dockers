from __future__ import absolute_import
from .common import *

# Production overrides
DEBUG = False

TEMPLATE_DEBUG = DEBUG
PIPELINE = True

PIPELINE_CSS_COMPRESSOR = 'pipeline.compressors.NoopCompressor'
PIPELINE_JS_COMPRESSOR = 'pipeline.compressors.slimit.SlimItCompressor'

#because of lighttpd
FORCE_SCRIPT_NAME = ''

#todo: configure the productive database
DATABASES = {
    'default':
        {'ENGINE': 'django.db.backends.postgresql_psycopg2',
         'NAME': 'jnotaro_jeffersonnotaro',
         'HOST': 'localhost',
         'USER': 'jnotaro',
         'PASSWORD': 'TXArAdFIBp5D'
        }
}
