# -*- coding: utf-8 -*-
import os
gettext = lambda s: s
"""
Django settings for seelenbalsamweb project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
#BASE_DIR = os.path.dirname(os.path.dirname(__file__))
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SETTINGS_PATH = BASE_DIR
#import pdb
#pdb.set_trace()

env=os.environ.get
# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = env('SECRET_KEY', '-wtl4g*a_hks&p*gua=9&t*scs#jf^!#@=x)zl0zz_zl-rl613ETg@Wwe23%-')


ALLOWED_HOSTS = ['localhost', '127.0.0.1', 'jeffersonnotaro.ch', 'dev.jeffersonnotaro.ch', 'www.jeffersonnotaro.ch', '192.168.99.100']


# Application definition


ROOT_URLCONF = 'config.urls'

WSGI_APPLICATION = 'config.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases



# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'pt'

TIME_ZONE = 'America/Belem'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, '..', 'media')
STATIC_ROOT = os.path.join(BASE_DIR, '..', 'static')




################### DJANGO PIPELINE ######################

#STATICFILES_STORAGE = 'pipeline.storage.PipelineCachedStorage'

STATICFILES_FINDERS = (
     'django.contrib.staticfiles.finders.FileSystemFinder',
     'django.contrib.staticfiles.finders.AppDirectoriesFinder',
     #'pipeline.finders.PipelineFinder',
)
# STATICFILES_FINDERS = (
#     'pipeline.finders.FileSystemFinder',
#     'pipeline.finders.AppDirectoriesFinder',
#     'pipeline.finders.PipelineFinder',
#     'pipeline.finders.CachedFileFinder',
#     'django.contrib.staticfiles.finders.AppDirectoriesFinder',
# )

#
# PIPELINE = {
#     'STYLESHEETS': {
#         'core_css': {
#         'source_filenames': (
#                 #'css_src/assets/plugins/bootstrap/css/bootstrap.min.css',
#                 #'css_src/assets/css/font-awesome.css',
#                 #'css_src/assets/plugins/owl-carousel/owl.pack.css',
#                 #'css_src/assets/plugins/magnific-popup/magnific-popup.css',
#                 #'css_src/assets/plugins/jquery.bxslider/jquery.bxslider.css',
#
#             ),
#             'output_filename': 'css/core.css',
#             'extra_context': {
#                 'media': 'screen,projection',
#             },
#         },
#         'theme_css': {
#         'source_filenames': (
#                 #'css_src/assets/css/essentials.css',
#                 #'css_src/assets/css/layout.css',
#                 #'css_src/assets/css/color_scheme/andrea.css',
#
#             ),
#             'output_filename': 'css/theme.css',
#             'extra_context': {
#                 'media': 'screen,projection',
#             },
#         },
#         'print': {
#             'source_filenames': (
#                 #'css_src/print.scss',
#             ),
#             'output_filename': 'css/print.css',
#             'extra_context': {
#                 'media': 'print',
#             },
#         },
#     },
#     'JAVASCRIPT': {
#         'main_js': {
#         'source_filenames': (
#                 #'js_src/packages/jquery-1.11.2.min.js',
#                 #'js_src/packages/vendor/modernizr-2.6.2-respond-1.1.0.min.js',
#                 #'css_src/assets/js/*.js',
#                 #'js_src/plugins.js',
#                 #'js_src/base.js',
#             ),
#             'output_filename': 'js/jn_main.js',
#         },
#         'footer_js': {
#             'source_filenames': (
#                 #'css_src/assets/plugins/jquery-2.1.4.min.js',
# 		        #'css_src/assets/plugins/jquery.bxslider/jquery.bxslider.js',
# 		        #'css_src/assets/plugins/jquery.isotope.js',
# 		        #'css_src/assets/plugins/jquery.parallax-1.1.3.js',
# 		        #'css_src/assets/plugins/knob/js/jquery.knob.js',
# 		        #'css_src/assets/plugins/bootstrap/js/bootstrap.js',
# 		        #'css_src/assets/plugins/magnific-popup/jquery.magnific-popup.js',
# 		        #'css_src/assets/plugins/owl-carousel/owl.carousel.js',
# 		        #'css_src/assets/js/scripts.js',
#             ),
#             'output_filename': 'js/jn_footer.js',
#         },
#     }
# }


#PIPELINE_CSS_COMPRESSOR = 'pipeline.compressors.yuglify.YuglifyCompressor'
#PIPELINE_JS_COMPRESSOR = 'pipeline.compressors.yuglify.YuglifyCompressor'
# PIPELINE_COMPILERS = (
#     'pipeline.compilers.sass.SASSCompiler',
# )
################### PIPELINE END #########################

SITE_ID = 1

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    'django.template.loaders.eggs.Loader'
)


MIDDLEWARE_CLASSES = (
    'cms.middleware.utils.ApphookReloadMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.contrib.admindocs.middleware.XViewMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'cms.middleware.user.CurrentUserMiddleware',
    'cms.middleware.page.CurrentPageMiddleware',
    'cms.middleware.toolbar.ToolbarMiddleware',
    'cms.middleware.language.LanguageCookieMiddleware',
    #'pipeline.middleware.MinifyHTMLMiddleware',
)


TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, "templates"), ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.template.context_processors.csrf',
                'django.template.context_processors.request',
                'django.contrib.messages.context_processors.messages',
                'sekizai.context_processors.sekizai',
                'cms.context_processors.cms_settings',
            ],
        },
    },
]




INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'djangocms_admin_style',
    'django.contrib.admin',
    'django.contrib.sites',
    'django.contrib.sitemaps',
    'django.contrib.staticfiles',
    'django.contrib.messages',
    'cms',
    'mptt',
    'menus',
    'sekizai',
    'djangocms_text_ckeditor',
    'djangocms_style',
     #'djangocms_column',
     #'djangocms_file',
     #'djangocms_flash',
     #'djangocms_googlemap',
     #'djangocms_inherit',
     #'djangocms_link',
     #'djangocms_picture',
     #'djangocms_teaser',
     #'djangocms_video',
    # 'reversion',
    'treebeard',
    #'pipeline',
    'easy_thumbnails',
    'apps.ajaweb',
    #'apps.contributor',
)
SOUTH_MIGRATION_MODULES = {
    'image_gallery': 'image_gallery.south_migrations',
}
LANGUAGES = (
    ## Customize this
    ('pt', gettext('português')),
    ('en', gettext('english')),
    ('es', gettext('castellano')),
    ('de', gettext('deutsch')),
)




CMS_LANGUAGES = {
    1: [
        {
            'code': 'pt',
            'name': gettext('português'),
            'fallbacks': ['es', 'en'],
            'public': True,
            'hide_untranslated': True,
            'redirect_on_fallback':False,
        },
        {
            'code': 'en',
            'name': gettext('english'),
            'fallbacks': ['pt', 'es'],
            'public': True,
        },
        {
            'code': 'es',
            'name': gettext('castellano'),
            'public': False,
        },
        {
            'code': 'de',
            'name': gettext('Deutsch'),
            'public': True,
            'fallbacks': ['en'],
        },
    ],

    'default': {
        'fallbacks': ['pt', 'es', 'en'],
        'redirect_on_fallback':True,
        'public': True,
        'hide_untranslated': False,
    }
}

CMS_TEMPLATES = (
    ## Customize this
    ('fullwidth.html', 'Fullwidth'),
    ('sidebar_left.html', 'Sidebar Left'),
    ('sidebar_right.html', 'Sidebar Right'),
    ('article_sidebar_right.html', 'Article Sidebar Right')

)

CMS_PERMISSION = True

CMS_PLACEHOLDER_CONF = {}

DATABASES = {
    'default':
        {'ENGINE': 'django.db.backends.sqlite3', 'NAME': 'project.db', 'HOST': 'localhost', 'USER': '', 'PASSWORD': '', 'PORT': ''}
}



